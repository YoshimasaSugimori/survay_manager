import numpy as np

class PersonalityAnalyzer(object):
    sentences = [
        'バランス重視タイプ：チームの様子をうかがってよく人に合わせていく。何事も人間関係はバランスを保った関係を築けるチームの中心的存在。時々優柔不断になるので、決断は早めに。',
        '内向的で慎重派。会社の規律などをきちんと守れる堅実派。たまにチャンスを逃がしてしまう傾向があるので気を付けましょう。',
        'いろんな人との相性がよく、チャレンジ精神旺盛。企画やチームリーダーに向いているかもしれません。その一方進んで活動しすぎるので、たまにはチームの意見を尊重してみましょう。'
    ]
    rpb_z_centroids = np.array([
        [-0.2032, -0.1625, -0.7288, -0.1495,  0.0090],
        [ 1.0707, -0.7327,  0.4082, -0.4553, -0.7364],
        [-0.7467,  0.6302,  0.8495,  0.6813,  0.7184],
    ])
    ffi_median = np.array([27, 22, 26, 28, 27])
    ffi_made   = np.array([7.4130, 7.4130, 4.4478, 5.9304, 5.9304])
    neoac_ffi  = np.zeros(5)
    robz_neoac = np.zeros(5)
    cluster    = -1

    def __init__(self, values):
        self.neoac = np.array([
            values[3] + (8 - values[8]),
            values[0] + (8 - values[5]),
            values[4] + (8 - values[9]),
            values[6] + (8 - values[1]),
            values[2] + (8 - values[7]),
        ])
        self.clustering()

    def initial_convert(self):
        self.neoac_ffi[0] = np.round( 1.85*self.neoac[0] - 0.36*self.neoac[1] - 0.07*self.neoac[2] + 0.21*self.neoac[3] - 0.76*self.neoac[4] + 22.95)
        self.neoac_ffi[1] = np.round(-0.03*self.neoac[0] + 0.69*self.neoac[1] - 0.32*self.neoac[2] + 0.36*self.neoac[3] - 0.02*self.neoac[4] + 14.68)
        self.neoac_ffi[2] = np.round( 0.27*self.neoac[0] - 0.14*self.neoac[1] + 1.86*self.neoac[2] - 0.07*self.neoac[3] - 0.55*self.neoac[4] + 15.31)
        self.neoac_ffi[3] = np.round(-0.13*self.neoac[0] + 0.17*self.neoac[1] - 0.35*self.neoac[2] + 1.70*self.neoac[3] + 0.30*self.neoac[4] + 13.08)
        self.neoac_ffi[4] = np.round( 0.05*self.neoac[0] + 0.12*self.neoac[1] - 0.47*self.neoac[2] + 0.13*self.neoac[3] + 2.06*self.neoac[4] + 9.07)

    def calc_robz(self):
        self.robz_neoac = (self.neoac_ffi - self.ffi_median) / self.ffi_made

    def euclidean_dist(self):
        return np.array([
            np.linalg.norm(self.robz_neoac - self.rpb_z_centroids[0]),
            np.linalg.norm(self.robz_neoac - self.rpb_z_centroids[1]),
            np.linalg.norm(self.robz_neoac - self.rpb_z_centroids[2])
        ])

    def clustering(self):
        self.initial_convert()
        self.calc_robz()
        self.cluster = self.euclidean_dist().argmin()

if __name__ == '__main__':
    analyzer = Analyzer([1,3,4,5,3,2,6,3,1,7])
