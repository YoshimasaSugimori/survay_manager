from django.apps import AppConfig


class SmDashboardConfig(AppConfig):
    name = 'sm_dashboard'
