from django import template
register = template.Library()

@register.simple_tag
def show_alert(request):
    item = {
        'is_show' : False,
        'text'    : '',
        'type'    : 'success'
    }
    if 'alert_info' in request.session:
        item['is_show'] = True
        item['text'] = request.session['alert_info']['text']
        item['type'] = request.session['alert_info']['type']
        del request.session['alert_info']
    return item
