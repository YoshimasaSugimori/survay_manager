from django.shortcuts import render
from django.http.response import HttpResponse
from django.contrib.auth.decorators import login_required

@login_required
def index_template(request):
    return render(request, 'dashboard/index.html')
