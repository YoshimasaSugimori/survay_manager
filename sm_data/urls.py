from django.urls import path
from . import views

app_name = 'data'

urlpatterns = [
    path('regist/', views.regist_data, name='regist'),
]
