# Generated by Django 2.0.6 on 2018-06-17 14:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SurvayAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.IntegerField(default=0, verbose_name='カテゴリー')),
                ('question', models.IntegerField(default=0, verbose_name='質問番号')),
                ('value', models.IntegerField(default=0, verbose_name='回答')),
                ('result', models.IntegerField(default=0, verbose_name='結果')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='作成日時')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='更新日時')),
            ],
            options={
                'db_table': 'survay_answer',
                'verbose_name': 'アンケートデータ',
            },
        ),
        migrations.CreateModel(
            name='SurvayData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.UUIDField(blank=True, default=None, null=True)),
                ('personal_code', models.CharField(max_length=255, verbose_name='個人コード')),
                ('survay_id', models.IntegerField(default=0, verbose_name='アンケートID')),
                ('version_id', models.IntegerField(default=0, verbose_name='バージョンID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='作成日時')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='更新日時')),
                ('answers', models.ManyToManyField(to='sm_data.SurvayAnswer')),
            ],
            options={
                'db_table': 'survay_data',
                'verbose_name': 'アンケートデータ',
            },
        ),
    ]
