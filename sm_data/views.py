import json
from django.shortcuts import render
from django.http import Http404,HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .apps   import SmDataConfig
from .models import (
    SurvayData,
    SurvayAnswer,
)
from lib.analysis import PersonalityAnalyzer

@csrf_exempt
def regist_data(request):
    if request.method != 'POST':
        raise Http404

    # post_data = json.loads(request.body.decode('utf-8'))
    post_data = json.loads(request.POST['data'])
    if 'api_token' not in post_data or 'api_secret' not in post_data:
        raise Http404

    if post_data['api_token'] != SmDataConfig.API_TOKEN or post_data['api_secret'] !=  SmDataConfig.API_SECRET:
        raise Http404

    answers       = post_data['answers']
    survay_id     = post_data['survay_id']
    version_id    = post_data['version_id']
    personal_code = post_data['personal_code']

    survay_data = SurvayData(
        personal_code=personal_code,
        survay_id=survay_id,
        version_id=version_id
    )
    survay_data.save()

    for category in answers:
        for question in answers[category]:
            answer = SurvayAnswer(
                category=category,
                question=question,
                value=answers[category][question]
            )
            answer.save()
            survay_data.answers.add(answer)

    values   = [int(answers['10'][str(i)]) for i in range(1, 11)]
    analyzer = PersonalityAnalyzer(values)

    survay_data.result = analyzer.cluster
    survay_data.save()
    return JsonResponse({
        'sentence' : analyzer.sentences[analyzer.cluster],
        'dist'     : analyzer.euclidean_dist().tolist()
    })
