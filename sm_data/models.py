from django.db             import models
from soft_delete_it.models import SoftDeleteModel


class SurvayAnswer(models.Model):
    class Meta:
        verbose_name = 'アンケートデータ'
        db_table     = 'survay_answer'
    category   = models.IntegerField(default=0, verbose_name='カテゴリー')
    question   = models.IntegerField(default=0, verbose_name='質問番号')
    value      = models.IntegerField(default=0, verbose_name='回答')
    result     = models.IntegerField(default=0, verbose_name='結果')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="作成日時")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新日時")

class SurvayData(SoftDeleteModel):
    class Meta:
        verbose_name = 'アンケートデータ'
        db_table     = 'survay_data'
    personal_code = models.CharField(max_length=255, null=False, verbose_name="個人コード")
    survay_id     = models.IntegerField(default=0, verbose_name='アンケートID')
    version_id    = models.IntegerField(default=0, verbose_name='バージョンID')
    answers       = models.ManyToManyField(SurvayAnswer)
    created_at    = models.DateTimeField(auto_now_add=True, verbose_name="作成日時")
    updated_at    = models.DateTimeField(auto_now=True, verbose_name="更新日時")
