from django import template
register = template.Library()

@register.simple_tag
def get_newest_version(survay):
    version = survay.versions.all().order_by('-created_at').first()
    return version.number

@register.simple_tag
def get_question_count(survay):
    temp    = 0
    version = survay.versions.all().order_by('-created_at').first()
    for category in version.categories.all():
        temp = temp + len(category.questions.all())
    return temp

@register.simple_tag
def get_categories(survay):
    version = survay.versions.all().order_by('-created_at').first()
    return version.categories.all().order_by('number')

@register.simple_tag
def get_questions(category):
    return category.questions.all().order_by('number')

@register.simple_tag
def get_clusters(survay):
    return survay.clusters.all().order_by('number')

@register.simple_tag
def get_question_type(question):
    return question.get_type_display()

@register.simple_tag
def get_status_tag(log):
    if log.get_status_display() == 'Under_Review':
        return '<span class="label label-warning">申請中</span>'
    elif log.get_status_display() == 'Completed':
        return '<span class="label label-success">承認済み</span>'
    if log.get_status_display() == 'Reject':
        return '<span class="label label-danger">却下</span>'
