from django.db             import models
from soft_delete_it.models import SoftDeleteModel

class SurvayConstants(object):
    TYPE_CHOICES = (
        (1, 'YES or NO'),
        (2, 'Level'),
    )
    UPDATE_STATUS_CHOICES = (
        (1, 'Under_Review'),
        (2, 'Completed'),
        (3, 'Reject'),
    )
    UPDATE_TYPE_CHOICES = (
        (1, 'Survay'),
        (2, 'Cluster'),
    )

    @classmethod
    def get_type_index(cls, value):
        inverse = {v:k for k, v in dict(cls.TYPE_CHOICES).items()}
        return inverse[value]

    @classmethod
    def get_update_status_index(cls, value):
        inverse = {v:k for k, v in dict(cls.UPDATE_STATUS_CHOICES).items()}
        return inverse[value]


class Cluster(SoftDeleteModel):
    class Meta:
        verbose_name = 'クラスタ'
        db_table     = 'clusters'

    number     = models.SmallIntegerField(default=0, verbose_name='番号')
    text       = models.CharField(max_length=500, null=False, verbose_name="質問内容")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="作成日時")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新日時")

    def __str__(self):
        return self.text


class Question(models.Model):
    class Meta:
        verbose_name = '質問'
        db_table     = 'questions'
        # indexes      = [
        #     models.Index(fields=['id'])]

    number     = models.SmallIntegerField(default=0, verbose_name='番号')
    text       = models.CharField(max_length=255, null=False, verbose_name="質問内容")
    type       = models.SmallIntegerField(default=0, verbose_name='タイプ', choices=SurvayConstants.TYPE_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="作成日時")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新日時")

    def __str__(self):
        return self.text


class Category(models.Model):
    class Meta:
        verbose_name = 'カテゴリ'
        db_table     = 'category'
        # indexes      = [
        #     models.Index(fields=['id'])]

    number     = models.SmallIntegerField(default=0, verbose_name='番号')
    text       = models.CharField(max_length=255, null=False, verbose_name="カテゴリ内容")
    page       = models.SmallIntegerField(default=0, verbose_name='ページ')
    questions  = models.ManyToManyField(Question)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="作成日時")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新日時")

    def __str__(self):
        return self.text


class Version(models.Model):
    class Meta:
        verbose_name = 'バージョン'
        db_table     = 'versions'
        # indexes      = [
        #     models.Index(fields=['id'])]

    number     = models.SmallIntegerField(default=1, verbose_name='バージョン')
    categories = models.ManyToManyField(Category)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="作成日時")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新日時")

    def __str__(self):
        return self.number


class Survay(SoftDeleteModel):
    class Meta:
        verbose_name = 'アンケート'
        db_table     = 'survays'
        # indexes      = [
        #     models.Index(fields=['id'])]

    title      = models.CharField(max_length=255, null=False, verbose_name="アンケート名")
    path       = models.CharField(max_length=255, default='', verbose_name="ファイルパス")
    versions   = models.ManyToManyField(Version)
    clusters   = models.ManyToManyField(Cluster)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="作成日時")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新日時")

    def __str__(self):
        return self.title


class ClusterUpdateLog(SoftDeleteModel):
    class Meta:
        verbose_name = 'クラスタ更新ログ'
        db_table     = 'cluster_update_logs'
        # indexes      = [
        #     models.Index(fields=['id'])]

    cluster        = models.ForeignKey(Cluster, on_delete=models.DO_NOTHING)
    text           = models.CharField(max_length=500, default='', verbose_name="変更後文章")
    status         = models.SmallIntegerField(default=0, verbose_name='申請ステータス', choices=SurvayConstants.UPDATE_STATUS_CHOICES)
    update_comment = models.CharField(max_length=500, default='', verbose_name="更新コメント")
    reject_comment = models.CharField(max_length=500, default='', verbose_name="却下理由")
    created_at     = models.DateTimeField(auto_now_add=True, verbose_name="作成日時")
    updated_at     = models.DateTimeField(auto_now=True, verbose_name="更新日時")
