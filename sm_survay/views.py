import os,openpyxl,hashlib
import urllib.parse
from django.http import Http404, JsonResponse
from django.http.response import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import (
    render,
    redirect,
    get_object_or_404,
)
from .models import (
    Survay,
    Version,
    Category,
    Question,
    Cluster,
    ClusterUpdateLog,
    SurvayConstants,
)

@login_required
def index_template(request):
    survays = Survay.objects.all()
    context = {
        'survays' : survays
    }
    return render(request, 'survay/index.html', context)

@login_required
def detail_template(request, pk):
    survay = get_object_or_404(Survay, pk=pk)
    context = {
        'survay' : survay
    }
    return render(request, 'survay/detail.html', context)

@login_required
def create_template(request):
    if request.method != 'POST':
        return render(request, 'survay/create.html')
    else:
        file  = request.FILES['survay_file']
        title = request.POST['title']
        path  = __create_temp_file(file)
        survay, clusters = __parse_xlsx_file(path)
        __regist_servay(title, path, survay, clusters)
        request.session['alert_info'] = {
            'text' : '新規レコードの登録が完了しました。',
            'type' : 'success'
        }
        return redirect('survay:index')

@login_required
def edit_template(request, pk):
    survay = get_object_or_404(Survay, pk=pk)
    if request.method != 'POST':
        return Http404
    else:
        current = None
        for c in survay.clusters.all():
            if c.number == int(request.POST['number']):
                current = c
                break
        log = ClusterUpdateLog()
        log.cluster_id = current.id
        log.update_comment = request.POST['comment']
        log.text    = request.POST['text']
        log.status  = SurvayConstants.get_update_status_index('Under_Review')
        log.save()

        return JsonResponse({'result' : 'ok'})


@login_required
def delete_survay(request, pk):
    survay = get_object_or_404(Survay, pk=pk)
    if request.method != 'POST':
        survay.delete()
        request.session['alert_info'] = {
            'text' : '対象レコードを削除しました。',
            'type' : 'success'
        }
        return redirect('survay:index')
    raise Http404

@login_required
def history_template(request):
    if request.method != 'POST':
        context = {
            'logs' : ClusterUpdateLog.objects.all()
        }
        return render(request, 'survay/history.html', context)
    raise Http404

@login_required
def delete_cluster_log(request, pk):
    log = get_object_or_404(ClusterUpdateLog, pk=pk)
    log.delete()
    request.session['alert_info'] = {
        'text' : '対象の申請を取り下げました。',
        'type' : 'success'
    }
    return redirect('survay:history')

@login_required
def download(request, pk):
    survay = get_object_or_404(Survay, pk=pk)
    if request.method != 'POST' and os.path.exists(survay.path):
        with open(survay.path, 'rb') as fh:
            file_name = '{}.{}'.format(survay.title, survay.path.split('.')[-1])
            response  = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename={}'.format(urllib.parse.quote(file_name))
            return response
    raise Http404

def __create_temp_file(file):
    file_name = '{}.{}'.format(hashlib.md5(file.name.encode('utf-8')).hexdigest(), file.name.split('.')[-1])
    file_path = os.path.join(os.path.dirname(__file__), 'temp', file_name)
    with open(file_path, 'wb') as f:
        for chunk in file.chunks():
            f.write(chunk)
    return file_path

def __parse_xlsx_file(path):
    wb    = openpyxl.load_workbook(path)
    sheet = wb['survay']
    survay  = {}
    items = list(sheet.values)
    for i in range(len(items)):
        if i > 0:
            if items[i][0] not in survay:
                survay[items[i][0]] = {}

            if items[i][1] == 0:
                survay[items[i][0]]['text'] = items[i][2]
                survay[items[i][0]]['page'] = items[i][4]
            else:
                survay[items[i][0]][items[i][1]] = {
                    'text' : items[i][2],
                    'type' : SurvayConstants.get_type_index(items[i][3])
                }

    sheet    = wb['cluster']
    clusters = []
    items    = list(sheet.values)
    for i in range(len(items)):
        if i > 0:
            clusters.append({'number' : items[i][0], 'text' : items[i][1]})

    return survay, clusters

def __regist_servay(title, path, items, clusters):
    survay  = Survay(title=title, path=path)
    version = Version()
    survay.save()
    version.save()
    for category_number in items:
        category = Category(
            number=int(category_number),
            text=items[category_number]['text'],
            page=items[category_number]['page']
        )
        category.save()
        del items[category_number]['text']
        del items[category_number]['page']
        for question_number in items[category_number]:
            question = Question(
                number=int(question_number),
                text=items[category_number][question_number]['text'],
                type=items[category_number][question_number]['type']
            )
            question.save()
            category.questions.add(question)
        category.save()
        version.categories.add(category)
    version.save()
    survay.versions.add(version)

    for value in clusters:
        cluster = Cluster(**value)
        cluster.save()
        survay.clusters.add(cluster)

    survay.save()
