from django.urls import path
from . import views

app_name = 'survay'

urlpatterns = [
    path('index/', views.index_template, name='index'),
    path('create/', views.create_template, name='create'),
    path('history/', views.history_template, name='history'),
    path('detail/<int:pk>', views.detail_template, name='detail'),
    path('edit/<int:pk>', views.edit_template, name='edit'),
    path('delete/<int:pk>', views.delete_survay, name='delete'),
    path('delete/cluster_log/<int:pk>', views.delete_cluster_log, name='delete_cluster_log'),
    path('download/<int:pk>', views.download, name='download'),
]
